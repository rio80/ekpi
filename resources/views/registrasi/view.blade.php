@extends('layouts.template')
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ url('/'.$jenis) }}">Registrasi Tabel</a></li>
<li class="breadcrumb-item active">{{ $jenis }}</li>
@endsection
@section('content')
<div class="row">
    <div class="table-responsive col-sm-8 col-md-8">
        <table class="table">
            <thead>
                <tr>
                    <td width="20%">
                        Nama Instansi :
                    </td>
                    <td>
                        {{ $nama_instansi }}
                    </td>
                </tr>
                <tr>
                    <td>
                        Pejabat Instansi :
                    </td>
                    <td>
                        {{ $pejabat_instansi }}
                    </td>
                </tr>
                <tr>
                    <td>
                        Email Instansi :
                    </td>
                    <td>
                        {{ $email }}
                    </td>
                </tr>
                <tr>
                    <td>
                        Alamat Instansi :
                    </td>
                    <td>
                        {{ $alamat }}
                    </td>
                </tr>
                <tr>
                    <td>
                        Kelurahan :
                    </td>
                    <td>
                        {{ $nama_kelurahan }}
                    </td>
                </tr>
                <tr>
                    <td>
                        Kecamatan :
                    </td>
                    <td>
                        {{ $nama_kecamatan }}
                    </td>
                </tr>
                <tr>
                    <td>
                        No Telp :
                    </td>
                    <td>
                        {{ $notelp }}
                    </td>
                </tr>
            </thead>
        </table>
    </div>
    <div class="col-sm-4 col-md-4" style="border:1px solid black;overflow:inherit;height:fit-content">
        {!! Form::label('foto', 'Foto '.$jenis, ['class' => 'form-control']) !!}
        <img src="{{ asset("$path_foto") }}" alt="" srcset=""
        width="100%" height="100%">
        </div>
</div>
<div class="row">
<a href="{{ url()->previous() }}" class="btn btn-info">Kembali</a>
</div>

@endsection
