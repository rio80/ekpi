@extends('layouts.template')
@section('breadcrumb')
    <li class="breadcrumb-item active">Tabel {{ $jenis_instansi }}</li>
@endsection
@section('content')
<div class="row">
        <a href="kelurahan/create" class="btn btn-info">Tambah Data {{ $jenis_instansi }}</a>
</div><br>
<div class="row">
    <div class="col-md-12">
        <div class="table-responsive">
            <table class="table table-registrasi table-bordered">
                <thead>
                    <tr>
                        <th>Nama</th>
                        <th>Pejabat Instansi</th>
                        <th>Email</th>
                        <th>Alamat</th>
                        <th>Kelurahan</th>
                        <th>Kecamatan</th>
                        <th>Kota</th>
                        <th>Provinsi</th>
                        <th>Kode Pos</th>
                        <th></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection
