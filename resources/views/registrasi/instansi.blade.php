@extends('layouts.template')
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ url('/'.$jenis) }}">Registrasi Tabel</a></li>
<li class="breadcrumb-item active">Registrasi {{ $jenis }}</li>
@endsection
@section('content')
<style>
 .h-divider{
 margin-top:5px;
 margin-bottom:5px;
 height:1px;
 width:100%;
 border-top:1px solid gray;
}
</style>
{!! Form::open(['url' => 'kelurahan', 'method' => 'post', 'files' => 'true']) !!}
@csrf
{!! Form::token() !!}
<div class="row">
    <div class="col-md-8 col-sm-12"> <h4>Personal Data</h4></div>
    <div class="h-divider"></div>

</div>
<div class="row">
<div class="col-md-8 col-sm-12">
        <div class="form-group">
            {!! Form::label('id_instansi', 'ID '.$jenis, ['class' => 'label-control']) !!}
            {!! Form::text('id_instansi', '', ['class' => 'form-control']) !!}
            {!! Form::hidden('id_jenis_instansi', $id, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('nama_instansi', 'Nama '.$jenis, ['class' => 'label-control']) !!}
            {!! Form::text('nama_instansi', '', ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('pejabat_instansi', 'Nama Pejabat', ['class' => 'label-control']) !!}
            {!! Form::text('pejabat_instansi', '', ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('email', 'Email', ['class' => 'label-control']) !!}
            {!! Form::email('email', '', ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('password', 'Password', ['class' => 'label-control']) !!}
            {!! Form::password('password', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4 col-sm-12">
        <div class="form-group">{!! Form::file('foto_kelurahan',
        ['class' => 'form-control', 'id' => 'foto_kelurahan']) !!}</div>
    </div>
</div>
<br>
    <div class="row">
        <div class="col-md-8 col-sm-12"> <h4>Informasi Alamat</h4></div>
         <div class="h-divider"></div>
     </div>
     <div class="row">
        <div class="col-md-8 col-sm-12">
            <div class="form-group">
                {!! Form::label('alamat', 'Alamat Kelurahan', ['class' => 'label-control']) !!}
                {!! Form::textarea('alamat', '', ['class' => 'form-control', 'rows' => '3', 'cols' => '10']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('lokasi_kelurahan', 'Kelurahan', ['class' => 'label-control']) !!}
                {!! Form::select('lokasi_kelurahan', $kelurahan, null, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('lokasi_kecamatan', 'Kecamatan', ['class' => 'label-control']) !!}
                {!! Form::select('lokasi_kecamatan', $kecamatan, null, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('kota_id', 'Kota', ['class' => 'label-control']) !!}
                {!! Form::select('kota_id', $kota, null, ['class' => 'form-control', 'placeholder' => '--Pilih Kota--']) !!}

            </div>
            <div class="form-group">
                {!! Form::label('provinsi', 'Provinsi', ['class' => 'label-control']) !!}
                {!! Form::select('provinsi', $provinsi, null, ['class' => 'form-control', 'placeholder' => '--Pilih Kota--']) !!}

            </div>
            <div class="form-group">
                {!! Form::label('kodepos', 'Kode Pos', ['class' => 'label-control']) !!}
                {!! Form::number('kodepos', '', ['class' => 'form-control col-md-6']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('notelp', 'No Telp', ['class' => 'label-control']) !!}
                {!! Form::text('notelp', '',['class' => 'form-control']) !!}
            </div>
        </div>
     </div>
     <div class="row">
        <div class="col-md-8 col-sm-12">
            <div class="form-group text-right">
                <button class="btn btn-success">Register</button>
            </div>
        </div>
     </div>
{!! Form::close() !!}
@endsection
