@extends('layouts.template')
@section('breadcrumb')
<li class="breadcrumb-item active">Tabel Kegiatan {{ $type }}</li>
@endsection
@section('content')
<div class="row">
        <a href="kegiatan/create" class="btn btn-info">Tambah Kegiatan</a>
</div><br>
<div class="row">
    <div class="col-md-12">
        <div class="table-responsive">
            <table class="table table-kegiatan table-bordered">
                <thead>
                    <tr>
                        <th>Nama</th>
                        <th>Kategori</th>
                        <th>Nama Instansi</th>
                        <th>Mulai</th>
                        <th>Selesai</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection
