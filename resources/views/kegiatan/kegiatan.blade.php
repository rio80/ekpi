@extends('layouts.template')
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ url('/'.$jenis) }}">Kegiatan Tabel</a></li>
<li class="breadcrumb-item active">Kegiatan {{ $type }}</li>
@endsection
@section('content')
<style>
 .h-divider{
 margin-top:5px;
 margin-bottom:5px;
 height:1px;
 width:100%;
 border-top:1px solid gray;
}
</style>
{!! Form::open(['url' => 'kegiatan', 'method' => 'post']) !!}
@csrf
{!! Form::token() !!}
<div class="row">
<div class="col-md-8 col-sm-12"> <h4>Input Kegiatan</h4></div>
    <div class="h-divider"></div>

</div>
<div class="row">
    <div class="col-md-8 col-sm-12">
        <div class="form-group">
            {!! Form::label('nama', 'Nama Kegiatan', ['class' => 'label-control']) !!}
            {!! Form::text('nama', '', ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('instansi', 'Jenis Instansi', ['class' => 'label-control']) !!}
            {!! Form::select('instansi', $instansi, null, ['class' => 'form-control', 'placeholder' => '--Pilih Instansi--', 'id' => 'instansi']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('register_instansi_id', 'Nama Instansi', ['class' => 'label-control']) !!}
            {!! Form::select('register_instansi_id', $instansi_id, null, ['class' => 'form-control', 'placeholder' => '--Pilih Instansi--', 'id' => 'register_instansi_id']) !!}

        </div>
        <div class="form-group">
            {!! Form::label('kategori_id', 'Kategori', ['class' => 'label-control']) !!}
            {!! Form::select('kategori_id', $kategori, null, ['class' => 'form-control', 'placeholder' => '--Pilih Kategori--']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('tgl_mulai', 'Tgl Mulai', ['class' => 'label-control']) !!}
            {!! Form::text('tgl_mulai', \Carbon\Carbon::now(),['class' => 'form-control col-sm-6', 'id' => 'tgl_mulai']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('tgl_selesai', 'Tgl Selesai', ['class' => 'label-control']) !!}
            {!! Form::text('tgl_selesai', \Carbon\Carbon::now(),['class' => 'form-control col-sm-6', 'id' => 'tgl_selesai']) !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-8 col-sm-12">
        <div class="form-group text-right">
            <button class="btn btn-success">Register</button>
        </div>
    </div>
 </div>

{!! Form::close() !!}
@endsection
