if ($('.table-registrasi').length > 0) {
    $('.table-registrasi').DataTable({
        ajax: 'instansi/data',
        columns: [{
                data: 'nama_instansi',
                name: 'nama_instansi'
            },
            {
                data: 'pejabat_instansi',
                name: 'pejabat_instansi'
            },
            {
                data: 'email',
                name: 'email'
            },
            {
                data: 'alamat',
                name: 'alamat'
            },
            {
                data: 'nama_kelurahan',
                name: 'nama_kelurahan'
            },
            {
                data: 'nama_kecamatan',
                name: 'nama_kecamatan'
            },
            {
                data: 'nama_kota',
                name: 'nama_kota'
            },
            {
                data: 'nama_provinsi',
                name: 'nama_provinsi'
            },
            {
                data: 'kodepos',
                name: 'kodepos'
            },
            {
                mRender:function(data, type, row) {
                    return '<a href="' +row.jenis_instansi + '/' +
                    row['id'] +
                    '" class="btn btn-info btn-sm">View</a>';
                }
            }
        ]
    });
}


if ($('.table-kegiatan').length > 0) {
    $('.table-kegiatan').DataTable({
        ajax: 'kegiatan/data',
        columns: [{
                data: 'nama',
                name: 'nama'
            },
            {
                data: 'nama_kategori',
                name: 'nama_kategori'
            },
            {
                data: 'nama_instansi',
                name: 'nama_instansi'
            },
            {
                data: 'tgl_mulai',
                name: 'tgl_mulai'
            },
            {
                data: 'tgl_selesai',
                name: 'tgl_selesai'
            },

        ]
    });
}

if ($('#instansi').length > 0) {
    $('#nama_kelurahan, #nama_kecamatan').hide();
    $('#instansi').on('change', function () {
        var selectInstansi = $(this).children("option:selected").val();
        var selectKelurahan = $('#nama_kelurahan');
        var selectKecamatan = $('#nama_kecamatan');

        if (selectInstansi == 'e051a533-88b5-4413-8108-30765e0efbcf') {
            selectKelurahan.show();
            selectKecamatan.hide();
        } else {
            selectKelurahan.hide();
            selectKecamatan.show();
        }
    });
}

if($('#tgl_mulai').length >= 1 && $('#tgl_selesai').length >=1){

    $('#tgl_mulai').datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
	    changeYear: true,
	    showButtonPanel: true,
    })
    $('#tgl_selesai').datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
	    changeYear: true,
	    showButtonPanel: true,
    })
}
