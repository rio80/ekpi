<?php

namespace App\Providers;

use App\Models\Instansi;
use App\Models\Kategori;
use App\Models\Kecamatan;
use App\Models\Kelurahan;
use App\Models\Kota;
use App\Models\Provinsi;
use App\Models\RegisterInstansi;
use Illuminate\Support\ServiceProvider;

class DropdownServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('registrasi.instansi', function($view){
            $view->with('kota', Kota::select('id_kota', 'nama')->pluck('nama', 'id_kota'));



            $view->with('kecamatan',  Kecamatan::select('id_kecamatan', 'nama')
            ->get()
            ->pluck('nama', 'id_kecamatan'));

            $view->with('kelurahan',  Kelurahan::select('id_kelurahan', 'nama')
            ->get()
            ->pluck('nama', 'id_kelurahan'));

            $view->with('provinsi',  Provinsi::select('id_provinsi', 'nama')
            ->get()
            ->pluck('nama', 'id_provinsi'));
        });

        view()->composer('kegiatan.kegiatan', function($view){
            $view->with('kategori', Kategori::select('id', 'nama')->pluck('nama', 'id'));
            $view->with('instansi', Instansi::select('id', 'nama')->pluck('nama', 'id'));
            $view->with('instansi_id', RegisterInstansi::select('id', 'nama_instansi')
            ->pluck('nama_instansi', 'id'));

        });
    }
}
