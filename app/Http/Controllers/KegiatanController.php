<?php

namespace App\Http\Controllers;

use App\Models\KegiatanInstansi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class KegiatanController extends Controller
{
 /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
 public function index()
 {
  $type_user = Auth::user()->type;
  session(['type_user' => $type_user]);

  if($type_user == 1){
    $type_user = 'kelurahan';
  }elseif($type_user == 2){
    $type_user = 'kecamatan';
  }

  $data = [
      'type' => $type_user,
  ];
  return view('kegiatan.index', $data);
 }

 public function table(Request $req)
 {
  if (session('type_user') == 1) {

  $show_kegiatan = collect(KegiatanInstansi::get());
  $show_kegiatan_type = $show_kegiatan->filter(function($v){
    return $v->nama_jenis == 'kelurahan';
  })->values()->all();

  } elseif (session('type_user') == 2) {

  $show_kegiatan = collect(KegiatanInstansi::get());
  $show_kegiatan_type = $show_kegiatan->filter(function($v){
    return $v->nama_jenis == 'kecamatan';
  })->values()->all();
  } else {
   $show_kegiatan_type = KegiatanInstansi::all();
  }
  $record_total = $show_kegiatan->count();

  $data = [
   'data' => $show_kegiatan_type,
   'recordsTotal' => $record_total,
   'recordsFiltered' => $record_total,
   'error' => 0,
  ];

  return $data;
 }

 /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
 public function create()
 {
  $url = request()->segment(1);

  $type_user = Auth::user()->type;

  if($type_user == 1){
    $type_user = 'kelurahan';
  }elseif($type_user == 2){
    $type_user = 'kecamatan';
  }
  $data = [
   'title' => 'Input Kegiatan',
   'jenis' => $url,
   'type' => $type_user,
  ];
  return view('kegiatan.kegiatan', $data);
 }

 /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
 public function store(Request $request)
 {
  KegiatanInstansi::create($request->all());
  return redirect('kegiatan');
 }

 /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
 public function show($id)
 {
  //
 }

 /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
 public function edit($id)
 {
  //
 }

 /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
 public function update(Request $request, $id)
 {
  //
 }

 /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
 public function destroy($id)
 {
  //
 }
}
