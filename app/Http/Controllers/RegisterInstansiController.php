<?php

namespace App\Http\Controllers;

use App\Models\Foto;
use App\Models\Instansi;
use App\Models\RegisterInstansi;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class RegisterInstansiController extends Controller
{
 /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */

 public function index()
 {
  $jenis_instansi = request()->segment(1);

  session(['jenis_instansi' => $jenis_instansi]);

  $data = [
   'title' => 'Tabel Registrasi ' . $jenis_instansi,
   'jenis_instansi' => $jenis_instansi,
  ];


  return view('registrasi.index', $data);
 }

 public function table(Request $req)
 {

  $jenis_instansi = session('jenis_instansi');

  $show_instansi = Instansi::where('nama', $jenis_instansi)->get()->first();
  $show_registrasi = RegisterInstansi::where('id_jenis_instansi', $show_instansi['id'])->get();

  $record_total = $show_registrasi->count();

  $draw = $req->get('draw');
  $data = [
   'data' => $show_registrasi,
   'recordsTotal' => $record_total,
   'recordsFiltered' => $record_total,
   'error' => 0,
  ];

  return $data;
 }

 /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
 public function create()
 {
  $jenis_instansi = session('jenis_instansi');
  $show_instansi = Instansi::where('nama', strtolower($jenis_instansi))->get()->first();

  $data = [
   'title' => 'Registrasi ' . $jenis_instansi,
   'jenis' => $jenis_instansi,
   'id' => $show_instansi['id'],
  ];
  return view('registrasi.instansi', $data);
 }

 /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
 public function store(Request $request)
 {

  $get_id_jenis_instansi = Instansi::where('id', $request->get('id_jenis_instansi'))
   ->get()->first();

  $jenis = session('jenis_instansi');
  $foto = $request->file('foto_' . $jenis);
  $foto_name = null;
  $upload_path = null;

  $foto_instansi = null;
  $type = null;

  if ($foto != null && $request->file('foto_' . $jenis)->isValid()) {
   $ext = $foto->getClientOriginalExtension();
   $foto_name = $request->get('id_instansi') . "_" . date('YmdHis') . ".$ext";
   $upload_path = 'uploadfoto';
   $request->file('foto_' . $jenis)->move($upload_path, $foto_name);
  }

  if (strtolower($get_id_jenis_instansi['nama']) == 'kelurahan') {
   $foto_instansi = 'kelurahan_id';
   $type = 1;
  } elseif (strtolower($get_id_jenis_instansi['nama']) == 'kecamatan') {
   $foto_instansi = 'kecamatan_id';
   $type = 2;
  }
  Foto::create([
   $foto_instansi => $request->get('id_instansi'),
   'nama' => $foto_name,
   'path' => $upload_path,
  ]);

  RegisterInstansi::create($request->all());

  User::create([
   'name' => $request->get('pejabat_instansi'),
   'email' => $request->get('email'),
   'password' => Hash::make($request->get('password')),
   'type' => $type,
  ]);
  return redirect($jenis);
 }

 /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
 public function show($id)
 {

  $getInstansi = RegisterInstansi::find($id);

  $get_id_jenis_instansi = Instansi::where('id', $getInstansi['id_jenis_instansi'])
   ->get()->first();

  $foto_instansi = null;
  $jenis_instansi = $get_id_jenis_instansi['nama'];
  $nama_instansi = $getInstansi['nama_instansi'];

  if (strtolower($jenis_instansi) == 'kelurahan') {
   $foto_instansi = 'kelurahan_id';
  } elseif (strtolower($jenis_instansi) == 'kecamatan') {
   $foto_instansi = 'kecamatan_id';
  }

  $path_foto = Foto::where($foto_instansi, $getInstansi['id_instansi'])->first();

  $getInstansi['title'] = 'Data ' . ucwords($jenis_instansi . " " . $nama_instansi);
  $getInstansi['jenis'] = session('jenis_instansi');
  $getInstansi['path_foto'] = $path_foto['path'] . "/" . $path_foto['nama'];

  return view('registrasi.view', $getInstansi);
 }

 /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
 public function edit($id)
 {
  //
 }

 /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
 public function update(Request $request, $id)
 {
  //
 }

 /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
 public function destroy($id)
 {
  //
 }
}
