<?php

namespace App\Http\Controllers;

use App\Models\Kelurahan;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class KelurahanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data = [
            'title' => 'Tabel Kelurahan',
        ];

        return view('registrasi.index', $data);
    }

    public function table(Request $req){
        $show_kelurahan = Kelurahan::get();
        $record_total = $show_kelurahan->count();

        $draw = $req->get('draw');
        $data = [
            'data' => $show_kelurahan,
            'recordsTotal' => $record_total,
            'recordsFiltered' => $record_total,
            'error' => 0,
        ];

        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'title' => 'Registrasi Kelurahan',
        ];
        return view('registrasi.kelurahan', $data);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        Kelurahan::create($request->all());

        User::create([
            'name' => $request->get('nama_lurah'),
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('password')),
        ]);

        return redirect('kelurahan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
