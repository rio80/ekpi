<?php

namespace App\Models;

use App\Traits\Uuid\Uuids;
use Illuminate\Database\Eloquent\Model;

class Instansi extends Model
{
    use Uuids;

    protected $table = 'instansi';

    protected $primaryKey = 'id';
    public $incrementing = false;


    protected $fillable = [
        'nama',
    ];
}
