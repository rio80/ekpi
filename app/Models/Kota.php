<?php

namespace App\Models;

use App\Traits\Uuid\Uuids;
use Illuminate\Database\Eloquent\Model;

class Kota extends Model
{
    use Uuids;
    public $incrementing = false;

    protected $fillable =[
        'id_kota',
        'nama'
    ];

    protected $table = 'kota';
    protected $primaryKey = 'id';

}
