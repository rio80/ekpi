<?php

namespace App\Models;

use App\Traits\Uuid\Uuids;
use Illuminate\Database\Eloquent\Model;

class KegiatanInstansi extends Model
{
    use Uuids;

    protected $table = 'kegiatan_instansi';

    public $incrementing = false;

    protected $primaryKey = 'id';

    protected $appends = ['nama_kategori', 'nama_instansi', 'nama_jenis'];


    protected $fillable = [
        'id',
        'nama',
        'kategori_id',
        'register_instansi_id',
        'tgl_mulai',
        'tgl_selesai'
    ];

    public function Kategori(){
        return $this->belongsTo('App\Models\Kategori', 'kategori_id');
    }

    public function RegisterInstansi(){
        return $this->belongsTo('App\Models\RegisterInstansi', 'register_instansi_id');
    }


    public function getNamaKategoriAttribute()
    {
        return $this->Kategori['nama'];
    }

    public function getNamaInstansiAttribute()
    {
        return $this->RegisterInstansi['nama_instansi'];
    }

    public function getNamaJenisAttribute()
    {
        return $this->RegisterInstansi['jenis_instansi'];
    }
}
