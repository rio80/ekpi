<?php

namespace App\Models;

use App\Traits\Uuid\Uuids;
use Illuminate\Database\Eloquent\Model;

class Foto extends Model
{
    use Uuids;

    protected $table = 'foto';

    protected $primaryKey = 'id';

    public $incrementing = false;

    protected $fillable = [
        'kelurahan_id', 'kecamatan_id', 'nama', 'path'
    ];
}
