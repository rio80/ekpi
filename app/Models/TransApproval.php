<?php

namespace App\Models;

use App\Traits\Uuid\Uuids;
use Illuminate\Database\Eloquent\Model;

class TransApproval extends Model
{
    use Uuids;

    public $incrementing = false;

    protected $table = 'trans_approval_instansi';

    protected $primaryKey = 'id';

    protected $fillable = [
        'approval_id',
        'kegiatan_id',
        'kegiatan_approval',
        'note',
    ];
}
