<?php

namespace App\Models;

use App\Traits\Uuid\Uuids;
use Illuminate\Database\Eloquent\Model;

class Approval extends Model
{
    use Uuids;

    protected $table = "approval";

    protected $primaryKey = 'id';

    public $incrementing = false;

    protected $fillable = ['nama'];
}
