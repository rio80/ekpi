<?php

namespace App\Models;

use App\Traits\Uuid\Uuids;
use Illuminate\Database\Eloquent\Model;

class RegisterInstansi extends Model
{
    use Uuids;

    public $incrementing = false;

    protected $table = 'registrasi_instansi';

    protected $primaryKey = 'id';

    protected $fillable = ['id', 'id_jenis_instansi', 'id_instansi',
        'nama_instansi', 'pejabat_instansi', 'email',
        'alamat', 'lokasi_instansi', 'lokasi_kelurahan',
        'lokasi_kecamatan', 'kota_id',
        'provinsi', 'kodepos',
        'notelp'];

    protected $appends = [
        'nama_kota',
        'jenis_instansi',
        'nama_kecamatan',
        'nama_kelurahan',
        'nama_provinsi',
    ];

    public function Kota()
    {
        return $this->belongsTo(Kota::class, 'kota_id', 'id_kota');
    }

    public function Kecamatan()
    {
        return $this->belongsTo(Kecamatan::class, 'lokasi_kecamatan', 'id_kecamatan');
    }

    public function Kelurahan()
    {
        return $this->belongsTo(Kelurahan::class, 'lokasi_kelurahan', 'id_kelurahan');
    }

    public function Provinsi()
    {
        return $this->belongsTo(Provinsi::class, 'provinsi', 'id_provinsi');
    }

    public function Instansi()
    {
        return $this->belongsTo(Instansi::class, 'id_jenis_instansi');
    }

    public function getNamaKotaAttribute()
    {
        return $this->Kota['nama'];
    }

    public function getNamaKecamatanAttribute()
    {
        return $this->Kecamatan['nama'];
    }

    public function getNamaKelurahanAttribute()
    {
        return $this->Kelurahan['nama'];
    }

    public function getNamaProvinsiAttribute()
    {
        return $this->Provinsi['nama'];
    }

    public function getJenisInstansiAttribute()
    {
        return strtolower($this->Instansi['nama']);
    }

}
