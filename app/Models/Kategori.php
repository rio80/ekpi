<?php

namespace App\Models;

use App\Traits\Uuid\Uuids;
use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    use Uuids;

    protected $table = 'kategori';

    protected $primaryKey = 'id';
    public $incrementing = false;


    protected $fillable = [
        'nama',
        'bobot'
    ];
}
