<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

use App\Models\Approval;
use App\Models\Kategori;
use App\Models\Kota;

Route::get('/', function () {
 return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth']], function () {

 Route::get('instansi/data', 'RegisterInstansiController@table');
 Route::resource('kelurahan', 'RegisterInstansiController');
 Route::resource('kecamatan', 'RegisterInstansiController');

 Route::get('kegiatan/data', 'KegiatanController@table');
 Route::resource('kegiatan', 'KegiatanController');
});


Route::get('/insert_approve', function () {
 Approval::truncate();
 $data = [
    [
        'nama' => 'setuju',
       ],
       [
         'nama' => 'tolak',
        ],
    ];

    foreach ($data as $key => $value) {
        Approval::create(
            ['nama' => $value['nama']]
        );
    }
});

// Route::get('/insert_kota', function () {
//  Kota::truncate();
//  return Kota::create(
//   [
//    'id_kota' => 'J001',
//    'nama' => 'Jakarta Timur',
//   ],
//  );
// });

// Route::get('/insert_kategori', function () {
//  Kategori::truncate();
//  return Kategori::create(
//   [
//    'nama' => 'Tertib Kaki Lima',
//    'bobot' => '20',
//   ],
//  );
// });

//    Route::get('/insert_instansi', function () {
//     // Instansi::truncate();
//     return Instansi::create(
//         [
//            'nama' => 'Kecamatan',
//           ],
//     );
//    });
