<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableKelurahan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('KELURAHAN', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('id_provinsi')->default('-');
            $table->string('id_kota')->default('-');
            $table->string('id_kecamatan')->default('-');
            $table->string('id_kelurahan')->default('-');
            $table->string('nama')->default('-');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('KELURAHAN');
    }
}
