<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRegistrasiInstansi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('REGISTRASI_INSTANSI', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('id_jenis_instansi', 255);
            $table->string('id_instansi', 255);
            // $table->string('id_kelurahan', 10);
            $table->string('nama_instansi', 100)->default('-');
            $table->string('pejabat_instansi', 100)->default('-');
            $table->string('email', 100)->default('-');
            $table->string('password', 100)->default('-');
            // $table->string('image_kelurahan', 255)->default('-');
            $table->text('alamat');
            $table->string('lokasi_kelurahan', 100)->default('-');
            $table->string('lokasi_kecamatan', 100)->default('-');
            $table->string('kota', 50)->default('-');
            $table->string('provinsi', 50)->default('-');
            $table->string('kodepos', 8)->default('-');
            $table->string('notelp', 14)->default('-');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('REGISTRASI_INSTANSI');
    }
}
