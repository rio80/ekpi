<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableKecamatan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('KECAMATAN', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('id_provinsi')->default('-');
            $table->string('id_kota')->default('-');
            $table->string('id_kecamatan')->default('-');
            $table->string('nama')->default('-');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('KECAMATAN');
    }
}
