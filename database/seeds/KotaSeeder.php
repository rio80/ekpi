<?php

use App\Models\Kota;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class KotaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kota')->insert(
            [
                'id_kota' => 'J001',
                'nama' => 'Jakarta Timur',
            ],
            [
                'id_kota' => 'J002',
                'nama' => 'Jakarta Barat',
            ],
            [
                'id_kota' => 'J003',
                'nama' => 'Jakarta Pusat',
            ]
        );
    }
}
